class Fibo:
    
    @staticmethod
    def getNth(n):
        a, b = 0, 1
        for i in range(0, n):
            a, b = b, a + b
        return a

    @staticmethod
    def checkIsItFiboElement(el):
        wyn = 0
        i=0
        while(wyn < el):
            wyn = getNth(i)
            i+=1

        if wyn == el:
            return True
        return False

