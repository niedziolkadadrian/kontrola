def getNth(n):
    a, b = 0, 1
    for i in range(0, n):
        a, b = b, a + b
    return a

tab = []
start = int(input())
for i in range(start, start+20):
    tab.append(getNth(i))
print(tab)