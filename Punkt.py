import math
class Punkt:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def getPunkt(self):
        return (self.x, self.y, self.z)

    def getLength(self):
        return math.sqrt(self.x*self.x+self.y*self.y+self.z*self.z)
